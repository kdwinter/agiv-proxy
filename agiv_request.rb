class AGIVRequest
  attr_reader :response
  attr_reader :original_response

  #GEWEST_ID = 2 # vlaanderen

  def initialize
    @client = Savon.client(:wsdl => 'http://crab.agiv.be/wscrab/wscrab.svc?wsdl', :env_namespace => 'soapenv', :namespace_identifier => 'crab', log: false)
  end

  def method_missing(operation, *args, &block)
    operation = operation.to_sym
    if @client.operations.include?(operation)
      begin
        data = extract_options(args)
        @response = @original_response = @client.call(operation, :message => data)
        @response = extract_response(operation)
      rescue Savon::SOAPFault => e
        Rails.logger.error "SOAP Error: #{e.message}"
      rescue Errno::ECONNRESET => e
        Rails.logger.error "ECONNRESET: #{e.message}"
      rescue Timeout::Error, Errno::ETIMEDOUT => e
        Rails.logger.error "Timeout: #{e.message}"
      end
    else
      Rails.logger.warn "Invalid SOAP operation: #{operation}"
      super
    end
  end

  def gemeenten(gewest_id = 2)
    list_gemeenten_by_gewest_id('crab:GewestId' => gewest_id.to_i)
  end

  def postcodes(gemeente_id)
    list_postkantons_by_gemeente_id('crab:GemeenteId' => gemeente_id.to_i) if gemeente_id
  end

  def straatnamen(gemeente_id)
    list_straatnamen_by_gemeente_id('crab:GemeenteId' => gemeente_id.to_i) if gemeente_id
  end

  def huisnummers(straatnaam_id)
    list_huisnummers_by_straatnaam_id('crab:StraatnaamId' => straatnaam_id.to_i) if straatnaam_id
  end

  def gemeente_info(gemeente_naam)
    gemeenten.find { |r| r[:gemeente_naam] == gemeente_naam }
  end

  def adresposities(huisnummer_id)
    list_adresposities_by_huisnummer_id('crab:HuisnummerId' => huisnummer_id.to_i) if huisnummer_id
  end

  def adrespositie(adrespositie_id)
    get_adrespositie_by_adrespositie_id('crab:AdrespositieId' => adrespositie_id.to_i) if adrespositie_id
  end

private

  def extract_response(operation)
    res = @response.body[:"#{operation}_response"][:"#{operation}_result"]
    if operation.to_s.start_with?('get')
      res
    else
      res[res.keys.first]
    end
  rescue => e
    @response.body
  end

  def extract_options(ary)
    ary.last.is_a?(::Hash) ? ary.pop : {}
  end
end
