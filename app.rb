require "bundler"
Bundler.require
require "./agiv_request.rb"

set :server, 'thin'

before do
  response.headers['Access-Control-Allow-Origin'] = '*'
  content_type :json
end

get '/' do
  {status: 'ok'}.to_json
end

get '/agiv' do
  begin
    agiv = AGIVRequest.new
    case params[:it] # info type
    when 'g'
      agiv.gemeenten(params[:g])
      response = agiv.response
    when 'p' # postcodes
      agiv.postcodes(params[:gid])
      response = agiv.response
      case response
      when Array
        response = response.map { |h| h[:postkanton_code] } rescue []
      when Hash
        response = Array(response[:postkanton_code])
      end
    when 's' # straatnamen
      agiv.straatnamen(params[:gid])
      response = agiv.response
    when 'h'
      agiv.huisnummers(params[:sid])
      response = agiv.response.map { |h| h[:huisnummer] } rescue []
    end

    if !agiv.response.nil? && (agiv.response.is_a?(Hash) || agiv.response.is_a?(Array))
      response.to_json
    else
      {}.to_json
    end
  rescue => e
    {sttaus: 'error', error: e.message}.to_json
  end
end
